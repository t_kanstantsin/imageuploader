<?php

/**
 * Image uploading module.
 * Class UploaderModule
 */
class UploaderModule extends CWebModule
{
    /**
     * @var string
     */
    public $defaultController = 'uploader';

    /**
     * In webroot
     * @var
     */
    public $imageUploadDir;

    /**
     * Days count
     * @var int
     */
    public $imageLifeTime = 30;

    /**
     * Max size for uploaded images.
     * Note: check server config.
     * @var int
     */
    public $maxFileSize = null; // no limit

    /**
     * Allowed image types
     * Note: on update @see $this->allowedMimeTypes
     * @var string
     */
    public $allowedTypes = 'jpg, gif, png, bmp';

    /**
     * Allowed image mimeTypes
     * Note: on update @see $this->allowedTypes
     * @var string
     */
    public $allowedMimeTypes = 'image/jpeg, image/gif, image/png, image/bmp';

    /**
     *
     */
    public function init()
    {
        if ($this->imageUploadDir == null
            || !file_exists(Yii::getPathOfAlias('webroot') . $this->imageUploadDir)
            || !is_writable(Yii::getPathOfAlias('webroot') . $this->imageUploadDir)
        ) {
            throw new CException('`ImageUploadDir` must be defined, folder exists and writable.');
        }

        // import the module-level models and components
        $this->setImport([
            'uploader.models.*',
            'uploader.components.*',
        ]);
    }

    /**
     * @param CController $controller
     * @param CAction $action
     * @return bool
     */
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }
}
