<?php

/**
 * Class ImageSaver
 */
class ImageSaver
{

    /**
     * @var ImageModel
     */
    private $_imageModel;

    /**
     * @param ImageModel $imageModel
     * @throws CException
     */
    public function __construct(ImageModel $imageModel)
    {
        if ($imageModel === null) {
            throw new CException('ImageModel cannot be null.');
        }

        $this->_imageModel = $imageModel;
    }

    /**
     * Saves image to db and filesystem
     * @return bool
     * @throws CDbException
     * @throws CException
     */
    public function save()
    {
        $this->_imageModel->imageFile = CUploadedFile::getInstance($this->_imageModel, 'imageFile');

        if ($this->_imageModel->validate(['imageFile'])) {
            $this->_imageModel->filename = $this->_imageModel->imageFile->name;
            $this->_imageModel->path_to_image = $this->generateImagePath();

            try {
                Yii::app()->db->beginTransaction();

                if ($this->_imageModel->save(true)) {
                    //$imageFileModel = new EasyImage($this->_imageModel->imageFile->tempName, Yii::app()->easyImage->driver);
                    $imagine = new Imagine\Imagick\Imagine();

                    // image itself
                    $imagine
                        ->open($this->_imageModel->imageFile->tempName)
                        ->save(Yii::getPathOfAlias('webroot') . $this->_imageModel->path_to_image, ['jpeg_quality' => 90, 'flatten' => 'false',]);
                    // their thumbnail
                    $imagine
                        ->open($this->_imageModel->imageFile->tempName)
                        ->thumbnail(new Imagine\Image\Box(100, 100), Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND)
                        ->save(Yii::getPathOfAlias('webroot') . $this->_imageModel->getThumbnailPath(), ['jpeg_quality' => 80]);
                } else {
                    Yii::app()->db->currentTransaction->rollback();

                    return false;
                }

                Yii::app()->db->currentTransaction->commit();
            } catch (Exception $e) {
                throw new CException($e->getMessage());
            } finally {
                if (Yii::app()->db->currentTransaction !== null) {
                    Yii::app()->db->currentTransaction->rollback();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Creates `path_to_file`
     * @return string
     */
    private function generateImagePath()
    {
        $basePath = Yii::getPathOfAlias('webroot');
        do {
            $fileName = uniqid() . '.' . pathinfo($this->_imageModel->imageFile->name, PATHINFO_EXTENSION);
            $dirName = substr($fileName, 0, 3);
            $dirPath = Yii::app()->getModule('uploader')->imageUploadDir . '/' . $dirName;
            $imagePath = $dirPath . '/' . $fileName;
        } while (file_exists($basePath . $imagePath));

        if (!file_exists($basePath . $dirPath)) {
            mkdir($basePath . $dirPath);
        }

        return $imagePath;
    }
}
