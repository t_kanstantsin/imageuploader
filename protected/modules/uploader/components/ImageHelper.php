<?php

/**.
 */
class ImageHelper
{
    /**
     * @param ImageModel $imageModel
     * @return bool
     */
    public static function isImageOutdated(ImageModel $imageModel)
    {
        return self::getRemainDays($imageModel) < 0;
    }

    /**
     * @param ImageModel $imageModel
     * @return int
     */
    public static function getRemainDays(ImageModel $imageModel)
    {
        $diff = time() - strtotime($imageModel->created_at);
        $remainDays = Yii::app()->getModule('uploader')->imageLifeTime - (int) round($diff / 60 / 60 / 24);

        return $remainDays;
    }

}
