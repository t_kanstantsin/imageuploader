<?php

/**
 * This is the model class for table "image".
 *
 * The followings are the available columns in table 'image':
 * @property integer $id
 * @property string $filename
 * @property string $path_to_image
 * @property string $created_at
 */
class ImageModel extends CActiveRecord
{
    /**
     * @var CUploadedFile
     */
    public $imageFile;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'image';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        /** @var UploaderModule $uploadModule */
        $uploadModule = Yii::app()->getModule('uploader');

        return [
            ['filename, path_to_image', 'required'],
            ['filename', 'length', 'max' => 100],
            ['path_to_image', 'length', 'max' => 1000],

            ['imageFile', 'file',
                'types' => $uploadModule->allowedTypes,
                'mimeTypes' => $uploadModule->allowedMimeTypes,
                'maxSize' => $uploadModule->maxFileSize,
                'allowEmpty' => false,
                'maxFiles' => 1,
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'path_to_image' => 'Path To Image',
            'created_at' => 'Created at',
        ];
    }

    public function beforeSave()
    {
        $this->created_at = date('Y-m-d H:i:s');

        return parent::beforeSave();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ImageModel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getThumbnailPath()
    {
        $extension = '.' . pathinfo($this->path_to_image, PATHINFO_EXTENSION);

        $path_to_thumb = substr($this->path_to_image, 0, strrpos($this->path_to_image, $extension)) . '_thumb' . $extension;

        return $path_to_thumb;
    }

}
