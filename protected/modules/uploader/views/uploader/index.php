<?php
/* @var $this UploaderController */
/* @var $model ImageModel */

/** @var CActiveForm $form */

$this->pageTitle = Yii::app()->name;

$this->breadcrumbs = [
    'Uploads',
];
?>

<h1>Uploader form</h1>

<span><b>NOTE</b>: max storage time is <?php echo Yii::app()->params['imageLifeTime']; ?> days.</span>

<br/>
<br/>

<?php $form = $this->beginWidget('CActiveForm', [
    'htmlOptions' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<?php echo $form->fileField($model, 'imageFile'); ?>
<?php echo CHtml::submitButton('Загрузить'); ?>

<?php $this->endWidget(); ?>


<div id="image-list">
    <a href="#" id="image-template" class="image-block"></a>
</div>


<?php Yii::app()->clientScript->registerScript('uploader-image-list', 'js:
    $(document).ready(function () {
        var cookie = $.cookie("' . $this->cookieVarName . '").replace(/\+/g, " ");
        var imageArray = JSON.parse(cookie);
        var imageListDom = document.getElementById("image-list");
        var imageListDomClone = imageListDom.cloneNode();
        var imageTemplateDom = document.getElementById("image-template");

        if (Array.isArray(imageArray)) {
            var outdatedIdArray = [];

            for (var i in imageArray) {
                if (isImageOutdated(imageArray[i])) {console.log("is outdated");
                    imageArray.splice(i, 1);
                    continue;
                }

                var imageDom = imageTemplateDom.cloneNode();
                imageDom.id = "";
                imageDom.href = imageArray[i].view_path;
                imageDom.title = "Go to " + imageArray[i].filename;
                imageDom.style.backgroundImage = "url(" + imageArray[i].thumbnail_path + ")";

                imageListDomClone.appendChild(imageDom);
            }

            imageListDom.parentNode.replaceChild(imageListDomClone, imageListDom);

            $.cookie("' . $this->cookieVarName . '", JSON.stringify(imageArray));
        }


        function isImageOutdated(image) {console.log(image.created_at);
            var imageDate = new Date(Date.parse(image.created_at));
            var today = new Date();
            imageDate.setDate(imageDate.getDate() + 30);

            return imageDate < today;
        }
    });
'); ?>
