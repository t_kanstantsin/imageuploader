<?php
/* @var $this UploaderController */
/* @var $model ImageModel */

$this->pageTitle = $model->filename . ' | ' . Yii::app()->name;


$this->breadcrumbs = [
    'Uploads' => $this->createUrl('index'),
    $model->filename,
];
?>

<h1>View image `<?php echo $model->filename; ?>`</h1>

<span><?php echo $model->getAttributeLabel('created_at'); ?>: <b><?php echo date_format(date_create($model->created_at), 'H:i, d M Y'); ?></b></span>
<br/>
<?php $remainDays = ImageHelper::getRemainDays($model); ?>
<span>Remain in storage: <b><?php echo $remainDays > 0 ? $remainDays : 0; ?></b> days</span>

<div id="image-view">
    <img src="<?php echo $model->path_to_image; ?>"
         alt="<?php echo $model->filename; ?>"/>
</div>
