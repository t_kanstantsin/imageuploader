<?php

/**
 * Class UploaderController
 */
class UploaderController extends Controller
{

    /**
     * @var string
     */
    public $cookieVarName = 'imageArray';

    /**
     * @var string
     */
    public $flashKey = 'file-upload-flash';

    /**
     * Form for file uploading and view all uploaded files
     */
    public function actionIndex()
    {
        $model = new ImageModel();

        $testModel = new ImageModel();
        $testModel->path_to_image = 'images/a.jpgsdf.jpg';

        if (Yii::app()->request->isPostRequest) {
            $model->attributes = $_POST['ImageModel'];

            $imageSaver = new ImageSaver($model);

            if ($imageSaver->save()) {
                Yii::app()->user->setFlash($this->flashKey, 'File upload successful.');
                $this->setCookie($model);

                $this->redirect($this->createUrl('view', ['id' => $model->id,]));
            }
        }

        $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $imageModel = ImageModel::model()->findByPk($id);

        if ($imageModel === null || !file_exists(Yii::getPathOfAlias('webroot') . $imageModel->path_to_image)) {
            throw new CHttpException(404, 'Image not found.');
        }

        $this->render('view', [
            'model' => $imageModel,
        ]);
    }

    /**
     * Adds saved image into cookie
     * @param ImageModel $imageModel
     */
    private function setCookie(ImageModel $imageModel)
    {
        $imageArrayCookie = Yii::app()->request->cookies[$this->cookieVarName];
        $imageArray = $imageArrayCookie === null ? [] : json_decode($imageArrayCookie->value, true);
        $imageArray = is_array($imageArray) ? $imageArray : [];

        array_unshift($imageArray, [
            'view_path' => $this->createUrl('view', ['id' => $imageModel->id,]),
            'filename' => $imageModel->filename,
            'thumbnail_path' => $imageModel->getThumbnailPath(),
            'created_at' => $imageModel->created_at,
        ]);

        Yii::app()->request->cookies[$this->cookieVarName] =
            new CHttpCookie($this->cookieVarName, json_encode($imageArray, JSON_UNESCAPED_SLASHES));
    }
}
