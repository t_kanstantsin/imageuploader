<?php

class m150317_065339_create_Image extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('image', [
            'id' => 'INT NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'filename' => 'VARCHAR(100) NOT NULL',
            'path_to_image' => 'VARCHAR(1000) NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('image');
    }
}