<?php

class m150320_071608_alter_Image_addColumn_createdAt extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('image', 'created_at', 'DATETIME NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('image', 'created_at');
    }
}