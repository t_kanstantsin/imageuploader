<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'ImageUploader',

    // preloading 'log' component
    'preload' => ['log'],

    'aliases' => [
        // global
        'app' => 'application',
        'vendor' => realpath(dirname(__FILE__) . '/../..') . '/vendor',
    ],

    // autoloading model and component classes
    'import' => [
        'application.models.*',
        'application.components.*',
    ],

    'modules' => [
        'uploader' => [
            'imageUploadDir' => '/uploads', // in webroot
            'imageLifeTime' => 30, // days
            'maxFileSize' => 2 * 1024 * 1024, // 2 MB
        ],
    ],

    // application components
    'components' => [

        'user' => [
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ],

        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/db.php'),

        'errorHandler' => [
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ],

        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ],
                [
                    'class' => 'CWebLogRoute',
                ],
            ],
        ],
    ],

    // application-level parameters that can be accessed
    'params' => require(dirname(__FILE__) . '/params.php'),
];
