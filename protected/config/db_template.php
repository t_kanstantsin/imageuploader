<?php

// This is the database connection configuration.
return [
    'connectionString' => 'mysql:host=localhost;dbname=imageUploader',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];